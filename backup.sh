#! /bin/sh

set -e
set -o pipefail

# set the filename from FILENAME env or default to ${MONGODB_DATABASE}_$(date +"%Y-%m-%dT%H:%M:%SZ").gz
FILENAME=${FILENAME:-${MONGODB_DATABASE}_$(date +"%Y-%m-%dT%H:%M:%SZ").gz}

if [ "${S3_ACCESS_KEY_ID}" = "**None**" ]; then
  echo "You need to set the S3_ACCESS_KEY_ID environment variable."
  exit 1
fi

if [ "${S3_SECRET_ACCESS_KEY}" = "**None**" ]; then
  echo "You need to set the S3_SECRET_ACCESS_KEY environment variable."
  exit 1
fi

if [ "${S3_BUCKET}" = "**None**" ]; then
  echo "You need to set the S3_BUCKET environment variable."
  exit 1
fi

if [ "${MONGODB_DATABASE}" = "**None**" ]; then
  echo "You need to set the MONGODB_DATABASE environment variable."
  exit 1
fi

if [ "${MONGODB_HOST}" = "**None**" ]; then
  if [ -n "${MONGODB_PORT_27017_TCP_ADDR}" ]; then
    MONGODB_HOST=$MONGODB_PORT_27017_TCP_ADDR
    MONGODB_PORT=$MONGODB_PORT_27017_TCP_PORT
  else
    echo "You need to set the MONGODB_HOST environment variable."
    exit 1
  fi
fi

if [ "${MONGODB_USER}" = "**None**" ]; then
  echo "You need to set the MONGODB_USER environment variable."
  exit 1
fi

if [ "${MONGODB_PASSWORD}" = "**None**" ]; then
  echo "You need to set the MONGODB_PASSWORD environment variable or link to a container named MONGODB."
  exit 1
fi

if [ "${S3_ENDPOINT}" == "**None**" ]; then
  AWS_ARGS=""
else
  AWS_ARGS="--endpoint-url ${S3_ENDPOINT}"
fi

# env vars needed for aws tools
export AWS_ACCESS_KEY_ID=$S3_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$S3_SECRET_ACCESS_KEY
export AWS_DEFAULT_REGION=$S3_REGION

echo "Creating dump of ${MONGODB_DATABASE} database from ${MONGODB_HOST}..."

mongodump --username $MONGODB_USER --password $MONGODB_PASSWORD --host $MONGODB_HOST --port $MONGODB_PORT --db $MONGODB_DATABASE --gzip --archive=dump.gz

echo "Uploading dump file $FILENAME to $S3_BUCKET"

cat dump.gz | aws $AWS_ARGS s3 cp - s3://$S3_BUCKET/$S3_PREFIX/$FILENAME || exit 2

echo "Database backup uploaded successfully"
