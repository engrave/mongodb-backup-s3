FROM alpine:3.15.3

RUN apk update

# install s3 tools
RUN apk add python3 py3-pip
RUN pip install awscli --upgrade --ignore-installed six

# install mongodump tools
RUN apk add mongodb-tools

# install go-cron
RUN apk add curl
RUN curl -L --insecure https://github.com/odise/go-cron/releases/download/v0.0.6/go-cron-linux.gz | zcat > /usr/local/bin/go-cron
RUN chmod u+x /usr/local/bin/go-cron
RUN apk del curl

# cleanup
RUN rm -rf /var/cache/apk/*

ENV MONGODB_DATABASE **None**
ENV MONGODB_HOST **None**
ENV MONGODB_PORT 27017
ENV MONGODB_USER **None**
ENV MONGODB_PASSWORD **None**
ENV MONGODB_EXTRA_OPTS ''
ENV S3_ACCESS_KEY_ID **None**
ENV S3_SECRET_ACCESS_KEY **None**
ENV S3_BUCKET **None**
ENV S3_REGION eu-central-1
ENV S3_PREFIX 'backup'
ENV S3_ENDPOINT **None**
ENV S3_S3V4 no
ENV SCHEDULE **None**

ADD run.sh run.sh
ADD backup.sh backup.sh

CMD ["sh", "run.sh"]
