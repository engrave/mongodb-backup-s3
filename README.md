# mongodb-backup-s3

Backup MongoDB database to S3 (supports periodic backups), built for mongodb 4.0

## Usage

Docker:
```sh
$ docker run -e S3_ACCESS_KEY_ID=key -e S3_SECRET_ACCESS_KEY=secret -e S3_BUCKET=my-bucket -e S3_PREFIX=backup -e MONGODB_DATABASE=dbname -e MONGODB_USER=user -e MONGODB_PASSWORD=password -e MONGODB_HOST=localhost registry.gitlab.com/engrave/mongodb-backup-s3
```

Docker Compose:
```yaml
mongodb:
  image: mongodb:4.0
  environment:
    MONGODB_USER: user
    MONGODB_PASSWORD: password

db-backup-s3:
  image: registry.gitlab.com/engrave/mongodb-backup-s3:latest
  links:
    - mongodb
  environment:
    SCHEDULE: '@daily'
    S3_REGION: region
    S3_ACCESS_KEY_ID: key
    S3_SECRET_ACCESS_KEY: secret
    S3_BUCKET: my-bucket
    S3_PREFIX: backup
    MONGODB_HOST: mongodb
    MONGODB_DATABASE: dbname
    MONGODB_USER: user
    MONGODB_PASSWORD: password
    FILENAME: mydb.gz # optional
```

Optional environment variables:
```
FILENAME: Filename of the backup (default: ${MONGODB_DATABASE}_$(date +"%Y-%m-%dT%H:%M:%SZ").gz, example mydb_2020-01-01T00:00:00Z.gz)
```

### S3 versioning with static filename

If you configure your s3 bucket to version your object, you can use static filename which will override the previous backup. This is useful if you want to keep only the latest backup. You can also configure some lifecycle rules to delete old backups.

### Automatic Periodic Backups

You can additionally set the `SCHEDULE` environment variable like `-e SCHEDULE="@daily"` to run the backup automatically.

More information about the scheduling can be found [here](http://godoc.org/github.com/robfig/cron#hdr-Predefined_schedules).

## How to restore

Use mongorestore to restore from S3:
1. Download file from S3
2. Unpack
3. `mongorestore --gzip --archive=<filename>`
